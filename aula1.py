
# -- Utilizaveis básicos --

# Usando print para mandar output para o usuario
#print("Hello world!")


# Variaveis
#frase = "hello world" #string qualquer coisa entre "" ou ''
#numero = 1 #integer qualquer numero
#numerosReais = 0.5 #float
#eVerdade = True #Bolean apenas True ou False


# Usando input para pegar dados do usuário
#nome = input("Digite seu nome")


## Operadores simples (lógica)
#+ - * /
# = recebe numero = 20  a variavél numero sera 20
# == igual (numero == 20) -> vai retronar True
# conversões do python
#converter algo para int int(algo)
#converter algo para float float(algo)
#converter algo para string str(algo)

# -- if --
# operador == no if
#Se um numero for igual a x, para isso usamos o operador == ->
#if numero == x:
#   print("é igual")

#Operador in no if
#Se tiver a palavra garantia em uma stringx, para isso usamos o operador in ->
#if "garantia" in stringx:
#   print("tem garantia")

#Operador and no if
#Se tiver tal condição e outra condição(duas validações dentro de 1 if), para isso usamos o operador and
#numero1 = 1
#numero2 = 2
#if (numero1 == 1 and numero2 == 2):
        #print("é igual")


#A Main serve para organizar o fluxo do cod
if __name__ == '__main__':
    # O input sempre retorna uma string
    # Usamos o int() para converter o input pra inteiro(numero)
    numero1 = int(input("Digite o numero 1: "))
    numero2 = int(input("Digite o numero 2: "))
    numero1 = int(input("Digite o numero 1: "))
    numero2 = int(input("Digite o numero 2: "))
    soma = numero1 + numero2
    print(soma) # poderia tambem ser print(numero1 + numero2)
    #Se por acaso quiser pirntar uma string + um inteiro converta o inteiro para string
    print("A soma dos numeros é: " + str(soma))

    #Usando o operador and
    if (numero1 == soma and numero2 == 1):
        print("é igual")

    # Pergunta é uma string
    pergunta = input("digite a pergunta:")

    #Se tiver garantia dentro da pergunta entra no if e printa
    if 'garantia' in pergunta:
        print("o produto tem garantia")
    #O print ok esta fora do if por conta da identação
    print("ok")


