#Exemplos while reviewer
'''
languages = ["pt", "es"]
language = ""
mode = ""
while language not in languages:
    language = input("Qual a linguagem das lojas desejadas?(es/pt)").lower()

modes = ["r", "s", "fr", "fs"]
while mode not in modes:
    mode = input(
        "r:  review\nrp: review pos \ns:  suggestion\nsp: suggestion pós\nfr: filtered review\nfs: filtered suggestion\n").lower()

#MyDoc são as sugestões/revisões
mydoc = [["sugestion1"], ["sugestion2"]]
count = 0
while True:
    if len(mydoc) == 0:
        print("Não contém suggestions ou reviews disponíveis.")
        #quebra o loop
        break

    if mode == "rp":
        print("imprimir rp")
    else:
        try:
            bot = {"workspace_id": mydoc[count]["wk"], "api_key": mydoc[count]["key"]}
        except:
            #passa para o proximo loop do msm while
            pass
        print("OAKLEY")
    count += 1
    if count > 20:
        break

#Exemplo simples de while
numero = 0
while numero < 1000000:
    numero = numero + 1
print(numero)

#Usando o simples para percorrer vetores:
nomes = ["Lukita- COmunist4 OKL", "Sté", "Thiaguinho", "xanos", "Yagão", "weslão", "comunista"]
i = 0
while i < len(nomes):
    if nomes[i] != "weslão":
        nomes[i] = "- Exercito vermelho"
    print(nomes[i])
    i += 1


#For para percorrer strings
for i in "lacoste":
    if i == "a":
        i = "4"
    print(i)

#For para percorrer vetores/dicts e fazer algo em cada um
for nome in nomes:
    if nome == "weslão":
        pass
    nome += "- Exercito vermelho _\|/_"
    print(nome)
'''
nomes = {"Lukita- COmunist4 OKL", "Sté", "Thiaguinho", "xanos", "Yagão", "weslão", "comunista"}
#For para percorrer vetores usando index na range do vetor
for i in range(len(nomes)):
    if nomes[i] == "Sté":
        nomes[i] = nomes[i+1]
    print(nomes[i])

#Try catch para tratar erros